package org.example.client;


import org.example.dto.Address;
import org.example.dto.Event;
import org.example.dto.Payload;

public interface Client {
    //блокирующий метод для чтения данных
    Event readData();

    //блокирующий метод отправки данных
    Result sendData(Address dest, Payload payload);
}
