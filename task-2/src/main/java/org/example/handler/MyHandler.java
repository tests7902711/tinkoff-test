package org.example.handler;


import org.example.client.Client;
import org.example.client.Result;
import org.example.dto.Address;
import org.example.dto.Payload;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MyHandler implements Handler {
    private final Client client;
    private final Duration timeout;
    private final Executor delayedExecutor;
    private final Executor executor;

    MyHandler(Client client, Duration timeout) {
        this.client = client;
        this.timeout = timeout;
        this.delayedExecutor = CompletableFuture.delayedExecutor(timeout.toMillis(), TimeUnit.MILLISECONDS);
        this.executor = Executors.newCachedThreadPool();
    }

    @Override
    public Duration timeout() {
        return timeout;
    }

    @Override
    public void performOperation() {
        List<Result> results = CompletableFuture.completedFuture(client.readData())
                .thenApply(event -> event.recipients().stream()
                        .distinct()
                        .map(
                            recipient -> this.sendDataWithRetry(recipient, event.payload())
                        )
                        .parallel()
                        .map(CompletableFuture::join)
                        .toList()
                )
                .join();
        // TODO remove (for test purposes)
        System.out.println(results);
    }

    private CompletableFuture<Result> sendDataWithRetry(Address recipient, Payload payload) {
        return CompletableFuture.supplyAsync(() -> client.sendData(recipient, payload), executor)
            .thenApplyAsync(result -> {
                // TODO remove (for test purposes)
                System.out.println(result);
                if (Result.REJECTED.equals(result)) {
                    throw new RuntimeException("rejected");
                }
                return result;
            })
            .exceptionallyCompose(ex ->
                CompletableFuture.supplyAsync(() -> sendDataWithRetry(recipient, payload), delayedExecutor).join()
            );
    }
}
