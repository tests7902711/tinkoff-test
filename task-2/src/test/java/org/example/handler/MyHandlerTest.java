package org.example.handler;

import org.example.client.Client;
import org.example.client.Result;
import org.example.dto.Address;
import org.example.dto.Event;
import org.example.dto.Payload;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

class TestClient implements Client {
    private final AtomicInteger counter = new AtomicInteger();

    @Override
    public Event readData() {
        return new Event(
            IntStream.range(1, 100)
                .mapToObj(i -> new Address("test datacenter", String.valueOf(i)))
                .toList(),
            new Payload("test", new byte[]{0, 0})
        );
    }

    @Override
    public Result sendData(Address dest, Payload payload) {
        System.out.println(Thread.currentThread().getName());
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        int i = counter.incrementAndGet();
        if (i % 2 == 0) {
            return Result.ACCEPTED;
        } else {
            return Result.REJECTED;
        }
    }
}

public class MyHandlerTest {

    private final Client client = new TestClient();
    private final Handler handler = new MyHandler(client, Duration.ofMillis(100));

    @Test
    void test() {
        handler.performOperation();
    }
}
